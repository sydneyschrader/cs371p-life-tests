*** Life<ConwayCell> 9x2 ***

Generation = 0, Population = 11.
**
.*
.*
..
**
..
*.
**
**

Generation = 1, Population = 11.
**
.*
..
**
..
**
**
..
**

Generation = 2, Population = 10.
**
**
**
..
..
**
**
..
..

Generation = 3, Population = 8.
**
..
**
..
..
**
**
..
..

*** Life<ConwayCell> 7x5 ***

Generation = 0, Population = 15.
.*..*
.**.*
***..
...*.
**...
**...
.**..

Generation = 1, Population = 10.
.***.
.....
*....
.....
***..
.....
***..

Generation = 2, Population = 6.
..*..
.**..
.....
*....
.*...
.....
.*...

*** Life<ConwayCell> 8x6 ***

Generation = 0, Population = 25.
*..*.*
*.**..
*.***.
**.***
*..**.
**..*.
.....*
*.*..*

Generation = 1, Population = 17.
.****.
*.....
*....*
*....*
......
**.***
*...**
......

Generation = 2, Population = 20.
.***..
*.***.
**....
......
**...*
**.*.*
**.*.*
......

Generation = 3, Population = 15.
.*..*.
*...*.
****..
......
***.*.
.....*
**....
......

*** Life<ConwayCell> 6x5 ***

Generation = 0, Population = 16.
*..**
.***.
*..**
*.**.
..*..
.*.**

Generation = 1, Population = 12.
.*.**
**...
*...*
..*.*
....*
..**.

Generation = 2, Population = 14.
***..
*****
*..*.
....*
..*.*
...*.

Generation = 3, Population = 6.
*....
....*
*....
....*
....*
...*.

Generation = 4, Population = 2.
.....
.....
.....
.....
...**
.....

Generation = 5, Population = 0.
.....
.....
.....
.....
.....
.....

*** Life<ConwayCell> 3x1 ***

Generation = 0, Population = 2.
.
*
*

Generation = 1, Population = 0.
.
.
.

Generation = 2, Population = 0.
.
.
.

Generation = 3, Population = 0.
.
.
.

Generation = 4, Population = 0.
.
.
.

*** Life<ConwayCell> 1x5 ***

Generation = 0, Population = 2.
..*.*

Generation = 1, Population = 0.
.....

Generation = 2, Population = 0.
.....

Generation = 3, Population = 0.
.....

Generation = 4, Population = 0.
.....

Generation = 5, Population = 0.
.....

Generation = 6, Population = 0.
.....

Generation = 7, Population = 0.
.....

Generation = 8, Population = 0.
.....

*** Life<ConwayCell> 1x8 ***

Generation = 0, Population = 2.
**......

Generation = 1, Population = 0.
........

Generation = 2, Population = 0.
........

*** Life<ConwayCell> 10x3 ***

Generation = 0, Population = 14.
..*
*..
***
*..
*.*
..*
...
*.*
.*.
*.*

Generation = 1, Population = 11.
...
*.*
*..
*.*
...
.*.
.*.
.*.
*.*
.*.

Generation = 2, Population = 13.
...
.*.
*..
.*.
.*.
...
***
***
*.*
.*.

Generation = 3, Population = 11.
...
...
**.
**.
...
*.*
*.*
...
*.*
.*.

Generation = 4, Population = 7.
...
...
**.
**.
*..
...
...
...
.*.
.*.

Generation = 5, Population = 4.
...
...
**.
...
**.
...
...
...
...
...

Generation = 6, Population = 0.
...
...
...
...
...
...
...
...
...
...

Generation = 7, Population = 0.
...
...
...
...
...
...
...
...
...
...

Generation = 8, Population = 0.
...
...
...
...
...
...
...
...
...
...

*** Life<ConwayCell> 9x10 ***

Generation = 0, Population = 52.
.***...***
*..*.**.*.
***..**.**
.*.**.**.*
**..*.**.*
..*..****.
...***...*
******..**
***.*....*

Generation = 1, Population = 32.
.****.****
*..*.*....
*........*
...**....*
**..*....*
.**......*
.........*
*.......**
*...**..**

Generation = 2, Population = 35.
.********.
*..*.***.*
...*......
**.**...**
**..*...**
***.....**
.*.......*
..........
........**

Generation = 3, Population = 28.
.***....*.
.*........
**.*.***.*
**.**...**
....*..*..
..*.......
***.....**
........**
..........

Generation = 4, Population = 30.
.**.......
...**.***.
...*.***.*
**.*.....*
.**.*...*.
..**....*.
.**.....**
.*......**
..........

Generation = 5, Population = 29.
..**...*..
...**...*.
...*.*...*
**.*.***.*
*...*...**
.......**.
.*.*...*..
.**.....**
..........

Generation = 6, Population = 29.
..***.....
........*.
...*.*.*.*
****.***.*
**..**...*
.......*.*
.*.....*.*
.**.....*.
..........

Generation = 7, Population = 29.
...*......
..*.....*.
.*.*.*.*.*
*..*...*.*
*..***.*.*
**....*..*
.**....*.*
.**.....*.
..........

*** Life<ConwayCell> 9x3 ***

Generation = 0, Population = 14.
**.
.*.
*.*
**.
.*.
..*
..*
.**
**.

Generation = 1, Population = 18.
**.
..*
*.*
*.*
***
.**
..*
*.*
***

Generation = 2, Population = 13.
.*.
*.*
..*
*.*
*..
*..
..*
*.*
*.*

Generation = 3, Population = 6.
.*.
..*
..*
...
*..
.*.
...
..*
...

Generation = 4, Population = 2.
...
.**
...
...
...
...
...
...
...

Generation = 5, Population = 0.
...
...
...
...
...
...
...
...
...

Generation = 6, Population = 0.
...
...
...
...
...
...
...
...
...

Generation = 7, Population = 0.
...
...
...
...
...
...
...
...
...

Generation = 8, Population = 0.
...
...
...
...
...
...
...
...
...

*** Life<ConwayCell> 8x9 ***

Generation = 0, Population = 42.
**.**....
**.**.***
*****.**.
*.*..*.**
*****....
.*...*..*
..*.*..**
*.******.

Generation = 1, Population = 31.
**.***.*.
......*.*
.........
.....*.**
*..******
*....*.**
..*.....*
.**.*****

*** Life<ConwayCell> 10x8 ***

Generation = 0, Population = 39.
..***.**
.**.**.*
.**.....
*....**.
**.****.
..**.*..
.****..*
.....*..
.*.*.*..
**.*.***

Generation = 1, Population = 32.
.**.*.**
....**.*
*.***...
*..*..*.
**.*....
*.......
.*...**.
.*...**.
**...*..
**...**.

Generation = 2, Population = 27.
...**.**
.......*
.**...*.
*.......
***.....
*.*.....
**...**.
.**.*...
..*.*...
**...**.

Generation = 3, Population = 22.
......**
..**.*.*
.*......
*.......
*.*.....
..*.....
*..*.*..
*.*.*...
*.*.*...
.*...*..

*** Life<ConwayCell> 5x1 ***

Generation = 0, Population = 2.
.
.
.
*
*

Generation = 1, Population = 0.
.
.
.
.
.

Generation = 2, Population = 0.
.
.
.
.
.

Generation = 3, Population = 0.
.
.
.
.
.

Generation = 4, Population = 0.
.
.
.
.
.

Generation = 5, Population = 0.
.
.
.
.
.

Generation = 6, Population = 0.
.
.
.
.
.

*** Life<ConwayCell> 5x1 ***

Generation = 0, Population = 3.
.
*
*
*
.

Generation = 1, Population = 1.
.
.
*
.
.

Generation = 2, Population = 0.
.
.
.
.
.

Generation = 3, Population = 0.
.
.
.
.
.

Generation = 4, Population = 0.
.
.
.
.
.

Generation = 5, Population = 0.
.
.
.
.
.

*** Life<ConwayCell> 4x1 ***

Generation = 0, Population = 2.
.
*
.
*

Generation = 1, Population = 0.
.
.
.
.

Generation = 2, Population = 0.
.
.
.
.

Generation = 3, Population = 0.
.
.
.
.

Generation = 4, Population = 0.
.
.
.
.

Generation = 5, Population = 0.
.
.
.
.

Generation = 6, Population = 0.
.
.
.
.

Generation = 7, Population = 0.
.
.
.
.

Generation = 8, Population = 0.
.
.
.
.

Generation = 9, Population = 0.
.
.
.
.

Generation = 10, Population = 0.
.
.
.
.

*** Life<ConwayCell> 3x3 ***

Generation = 0, Population = 2.
...
*.*
...

Generation = 1, Population = 0.
...
...
...

Generation = 2, Population = 0.
...
...
...

Generation = 3, Population = 0.
...
...
...

Generation = 4, Population = 0.
...
...
...

Generation = 5, Population = 0.
...
...
...

Generation = 6, Population = 0.
...
...
...

Generation = 7, Population = 0.
...
...
...

Generation = 8, Population = 0.
...
...
...

Generation = 9, Population = 0.
...
...
...

Generation = 10, Population = 0.
...
...
...

*** Life<ConwayCell> 6x4 ***

Generation = 0, Population = 14.
**.*
..*.
.*.*
*.**
...*
****

Generation = 1, Population = 12.
.**.
*..*
.*.*
.*.*
*...
.***

Generation = 2, Population = 13.
.**.
*..*
**.*
**..
*..*
.**.

Generation = 3, Population = 7.
.**.
*..*
....
....
*...
.**.

Generation = 4, Population = 6.
.**.
.**.
....
....
.*..
.*..

Generation = 5, Population = 4.
.**.
.**.
....
....
....
....

Generation = 6, Population = 4.
.**.
.**.
....
....
....
....

Generation = 7, Population = 4.
.**.
.**.
....
....
....
....

Generation = 8, Population = 4.
.**.
.**.
....
....
....
....

Generation = 9, Population = 4.
.**.
.**.
....
....
....
....

Generation = 10, Population = 4.
.**.
.**.
....
....
....
....

*** Life<ConwayCell> 10x6 ***

Generation = 0, Population = 29.
*.*...
.*....
.***..
***..*
*.*.*.
.**.**
.*..**
..*.**
.*.**.
.*.*.*

Generation = 1, Population = 15.
.*....
*..*..
...*..
*...*.
*...*.
*.*...
.*....
.**...
.*....
...*..

Generation = 2, Population = 13.
......
..*...
...**.
...**.
*..*..
*.....
*.....
***...
.*....
......

Generation = 3, Population = 14.
......
...*..
..*.*.
..*...
...**.
**....
*.....
*.*...
***...
......

Generation = 4, Population = 15.
......
...*..
..*...
..*.*.
.***..
**....
*.....
*.*...
*.*...
.*....

Generation = 5, Population = 9.
......
......
..*...
......
*..*..
*.....
*.....
*.....
*.*...
.*....

Generation = 6, Population = 7.
......
......
......
......
......
**....
**....
*.....
*.....
.*....

Generation = 7, Population = 5.
......
......
......
......
......
**....
......
*.....
**....
......

Generation = 8, Population = 6.
......
......
......
......
......
......
**....
**....
**....
......

*** Life<ConwayCell> 10x3 ***

Generation = 0, Population = 10.
...
..*
..*
..*
*..
*..
...
**.
.*.
*.*

Generation = 1, Population = 10.
...
...
.**
.*.
.*.
...
**.
**.
..*
.*.

*** Life<ConwayCell> 10x1 ***

Generation = 0, Population = 6.
.
*
*
*
*
*
.
*
.
.

Generation = 1, Population = 3.
.
.
*
*
*
.
.
.
.
.

Generation = 2, Population = 1.
.
.
.
*
.
.
.
.
.
.

Generation = 3, Population = 0.
.
.
.
.
.
.
.
.
.
.

Generation = 4, Population = 0.
.
.
.
.
.
.
.
.
.
.

Generation = 5, Population = 0.
.
.
.
.
.
.
.
.
.
.

Generation = 6, Population = 0.
.
.
.
.
.
.
.
.
.
.

*** Life<ConwayCell> 1x7 ***

Generation = 0, Population = 5.
***.**.

Generation = 1, Population = 1.
.*.....

*** Life<ConwayCell> 8x8 ***

Generation = 0, Population = 31.
.****.**
..*...**
..*.....
****.**.
*.*..***
.***...*
***....*
.*.*....

Generation = 1, Population = 24.
.***.***
.....***
.....*.*
*..***.*
*....*.*
...*...*
*.......
**......

Generation = 2, Population = 16.
..*.**.*
..*.....
.......*
.....*.*
...*.*.*
......*.
**......
**......

Generation = 3, Population = 14.
...*....
...*..*.
......*.
....*..*
....**.*
......*.
**......
**......

Generation = 4, Population = 14.
........
........
.....***
....*..*
....**.*
.....**.
**......
**......

Generation = 5, Population = 15.
........
......*.
.....***
....*..*
....*..*
....***.
**......
**......

*** Life<ConwayCell> 4x5 ***

Generation = 0, Population = 8.
..*..
***..
.**..
...**

Generation = 1, Population = 6.
..*..
*..*.
*....
..**.

Generation = 2, Population = 4.
.....
.*...
.***.
.....

Generation = 3, Population = 4.
.....
.*...
.**..
..*..

Generation = 4, Population = 6.
.....
.**..
.**..
.**..

Generation = 5, Population = 6.
.....
.**..
*..*.
.**..

Generation = 6, Population = 6.
.....
.**..
*..*.
.**..

Generation = 7, Population = 6.
.....
.**..
*..*.
.**..

Generation = 8, Population = 6.
.....
.**..
*..*.
.**..

Generation = 9, Population = 6.
.....
.**..
*..*.
.**..

Generation = 10, Population = 6.
.....
.**..
*..*.
.**..

*** Life<ConwayCell> 8x10 ***

Generation = 0, Population = 41.
*.*.......
*.*******.
.*.*.*.**.
***..**.*.
*....***.*
***.*..**.
**.*..*..*
*.**....*.

Generation = 1, Population = 24.
..*.****..
*....*..*.
.........*
*.*......*
...**....*
..***....*
....*....*
*.**......

Generation = 2, Population = 25.
....****..
....**.**.
.*......**
...*....**
.*..*...**
..*..*..**
.*..*.....
...*......

*** Life<ConwayCell> 7x3 ***

Generation = 0, Population = 12.
*..
***
..*
**.
*.*
.**
..*

Generation = 1, Population = 11.
*..
*.*
..*
*.*
*.*
..*
.**

Generation = 2, Population = 7.
.*.
...
..*
..*
..*
..*
.**

Generation = 3, Population = 7.
...
...
...
.**
.**
..*
.**

Generation = 4, Population = 4.
...
...
...
.**
...
...
.**

Generation = 5, Population = 0.
...
...
...
...
...
...
...

Generation = 6, Population = 0.
...
...
...
...
...
...
...

Generation = 7, Population = 0.
...
...
...
...
...
...
...

Generation = 8, Population = 0.
...
...
...
...
...
...
...
